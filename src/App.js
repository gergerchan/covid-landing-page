import { Container } from "reactstrap";
import Header from "./covid/Header";
import About from "./covid/About";
import Contagion from "./covid/Contagion";

function App() {
  return (
    <>
      <Container fluid="md">
        <Header />
        <About />
        <Contagion />
      </Container>
    </>
  );
}

export default App;

import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import Logo from "../asset/img/Logo.svg"
import illustration from "../asset/img/Header.svg"
import "../asset/css/header.scss"

const Header = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
  
    return (
      <div>
        <Navbar light expand="md" className="mb-5">
          <NavbarBrand href="/"><img src={Logo} alt="logo"/></NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} className="justify-content-end" navbar>
            <Nav className="mr-4" navbar>
              <NavItem className="mr-4">
                <NavLink href="#" className="active">Overview</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Contagion</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Symptoms</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Prevention</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" className="button-header px-4">Contact</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      
        <div className="row mt-5">
          <div className="col-6 mt-5">
            <p className="active title mb-1">COVID-19 Alert</p>
            <h1>Stay at Home Quarantine </h1>
            <h1 className="mb-4">To stop Corona virus</h1>
            <p className="subtitle mb-5">There is no specific medicine to prevent or treat coronavirus 
            disease (COVID-19). People may need supportive care to .</p>
            <a href="#next" className="button-title px-4 py-2">Let Us Help</a>
          </div>
          <div className="col-6">
            <img src={illustration} alt="" width="100%"/>
          </div>
        </div>

      </div>
    );
  }

export default Header

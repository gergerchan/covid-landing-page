import ImgContagion1 from '../asset/img/contagion-mask.svg'
import ImgContagion2 from '../asset/img/contagion-hand.svg'
import ImgContagion3 from '../asset/img/contagion-nose.svg'

export default function Contagion () {
  return (
    <div>
      <div className="text-center">
        <h3 className="about-title">Covid-19</h3>
        <h1>Contagion</h1>
        <p className="about-text mt-4">Corona viruses are a type of virus. There are many different kinds, and <br /> some cause disease. A newly identified type</p>
      </div>
      <div className="row">
        <div className="col-md-4">
          <div className="card-contagion">
            <img src={ImgContagion1} alt="img-contagion-one" className="img-fluid"/>
            <p className="">Air Transmission</p>
            <p>Objectively evolve tactical expertise before extensible initiatives. Efficiently simplify</p>
        </div>
        <div className="col-md-4">
          <div className="card-contagion">
            <img src={ImgContagion2} alt="img-contagion-one" className="img-fluid"/>
            <p className="">Air Transmission</p>
            <p>Objectively evolve tactical expertise before extensible initiatives. Efficiently simplify</p>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card-contagion">
            <img src={ImgContagion3} alt="img-contagion-one" className="img-fluid"/>
            <p className="">Air Transmission</p>
            <p>Objectively evolve tactical expertise before extensible initiatives. Efficiently simplify</p>
          </div>
        </div>
      </div>
      </div>
    </div>
  )
}

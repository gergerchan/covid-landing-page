import ImgAbout from "../asset/img/about-covid.svg"
import '../asset/css/about.scss'
function About (){
  return(
    <div className="container">
      <div className="row mt-4">
        <div className="col-md-6">
        <img src={ImgAbout} alt="img-about-covid" className="img-fluid img-about" />
        </div>
        <div className="col-md-6 mt-5">
          <h3 className="about-title" >What Is Covid-19</h3>
          <h1 className="about-sub-title" >Coronavirus</h1>
          <p className="about-text mb-5 mt-4">Corona viruses are a type of virus. There are many different kinds, and some cause disease. A newly identified type has caused a recent outbreak of respiratory illness now called COVID-19.Lauren Sauer, M.S., the director of operations with the Johns Hopkins Office of Critical Event Preparedness and Response</p>
          <a href="#next" className="btn-about px-4 py-2">Learn More</a>
        </div>
      </div>
    </div>
  )
}
export default About;